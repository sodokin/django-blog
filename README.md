#Django Blog



# Project Title
Django-blog

This is a blog project that lists the different episodes of the HARRY POTTER saga.
On this blog application, you can add, delete, modify and display the articles describing the episodes of the saga.


## Authors

- SODOKIN Koudjo


## Installation

To have the project on your computer, you have to clone the project from gitlab:   
git clone .......

Then you have to create a virtual environment: python3 -m venv my-env.

Then you have to activate the virtual environment: source venv/bin/ activate

You also have to create the PostgreSQL database, and give it the user rights :

    sudo su postgres

    psql

    create database booksblog;

    grant ALL privileges on database booksblog to the "user" name;

You have to install all the dependencies of the requirements.txt file: 

    pip install -r requirements.txt

Once the installations are done, you have to go to the /src folder to apply the migrations:

    python3 manage.py migrate

After creating the migrations, you have to register as a superuser :

    python3 manage.py createsuperuser

You can then go to 127.0.0.1:8000/admin to connect as administrator

To access the blog application, go to http://127.0.0.1:8000/blog/
